package org.hung.oauth2.web;

import java.security.Principal;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class UserInfoController {

    @RequestMapping("/userinfo")    
    public Object user(Principal principal) {
    	if (principal instanceof UsernamePasswordAuthenticationToken) {
    		return ((UsernamePasswordAuthenticationToken)principal).getPrincipal();
    	} else if (principal instanceof OAuth2Authentication) {
    		return ((OAuth2Authentication)principal).getUserAuthentication().getPrincipal();
    	} else {
    		return principal;
    	}
    }
	
}
