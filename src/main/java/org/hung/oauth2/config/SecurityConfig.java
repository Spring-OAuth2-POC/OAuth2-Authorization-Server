package org.hung.oauth2.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;

@Configuration
public class SecurityConfig  {
	
	/*@Bean
	public ActiveDirectoryLdapAuthenticationProvider activedirectoryLdapAuthProvider() {
		ActiveDirectoryLdapAuthenticationProvider activedirectoryLdapAuthProvider =
				new ActiveDirectoryLdapAuthenticationProvider(activeDirectoryDomain,activeDirectoryUrl);
		return activedirectoryLdapAuthProvider;
	}*/
	
	
	@EnableWebSecurity
	public static class WebMvcSecurityConfig extends WebSecurityConfigurerAdapter {
		
		@Value("${sfc.security.active-directory.domain:INTRA.HKSFC.ORG.HK}")	
		private String activeDirectoryDomain;
		          
		@Value("${sfc.security.active-directory.url:ldap://intra.hksfc.org.hk}")
		private String activeDirectoryUrl;
		
		@Override
		public void configure(WebSecurity web) throws Exception {
			web.debug(true);
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests()
				//.antMatchers("/oauth/authorize").au
				.anyRequest().authenticated()
				.and().httpBasic();
			//http.authorizeRequests().anyRequest().permitAll();
		}

		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			/*
			auth.inMemoryAuthentication()
				.withUser("user").password("{noop}abcd1234").roles("USER","ADMIN")
			.and()
				.withUser("john").password("{noop}abcd1234").roles("OPERATOR")
				//.and().withUser("client1").password("{noop}abcd1234").roles("CLIENT")
				;
			*/
			//auth.ldapAuthentication().userDetailsContextMapper(userDetailsContextMapper)
			ActiveDirectoryLdapAuthenticationProvider activedirectoryLdapAuthProvider =
					new ActiveDirectoryLdapAuthenticationProvider(activeDirectoryDomain,activeDirectoryUrl);
			
			activedirectoryLdapAuthProvider.setUserDetailsContextMapper(new ADUserDetailsMapper());
			activedirectoryLdapAuthProvider.
			auth.authenticationProvider(activedirectoryLdapAuthProvider);
		}
		
		@Bean(name="authenticationManager")
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}
		
	}	
}
