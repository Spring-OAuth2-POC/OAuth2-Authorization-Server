package org.hung.oauth2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
public class OAuth2ProviderConfig {

	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) {
			resources.resourceId("userinfo").stateless(false);
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {
			// @formatter:off
			http
			  // Since we want the protected resources to be accessible in the UI as well we need 
			  // session creation to be allowed (it's disabled by default in 2.0.6)
			  .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
			.and()
			  .requestMatchers().antMatchers("/userinfo")
			.and()
			  .authorizeRequests()
				.antMatchers("/userinfo").permitAll();//.access("#oauth2.hasScope('read')");				
			// @formatter:on
		}
	}

	@Configuration
	@EnableAuthorizationServer
	protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

		//@Autowired
		//private TokenStore tokenStore;

		@Autowired
		private AuthenticationManager authenticationManager;
		
		@Override
		public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
			security.checkTokenAccess("permitAll()");
			
		}

		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			// @formatter:off
			clients.inMemory()
			  .withClient("client1")
			    .secret("{noop}password")
			  	.resourceIds("userinfo","dummy")
				.authorizedGrantTypes("authorization_code")				
				.authorities("ROLE_USER")
				.scopes("read")
			  .and()
			  .withClient("client2")
				.secret("{noop}abcd1234")
				.resourceIds("dummy")
			  .and()
			  .withClient("client3")
			    .secret("{noop}password")
			  	//.resourceIds("userinfo","dummy")
				.authorizedGrantTypes("implicit")				
				.authorities("ROLE_USER")
				.scopes("read")
				.autoApprove(true)
			  .and()
			  .withClient("client4")
			    .secret("{noop}password")
			  	//.resourceIds("userinfo","dummy")
				.authorizedGrantTypes("authorization_code","refresh_token")				
				.authorities("ROLE_USER")
				.scopes("read")
				.autoApprove(true);
			
			// @formatter:on
		}

		//@Bean
		//public TokenStore tokenStore() {
		//	return new InMemoryTokenStore();
		//}

	}
	
}
